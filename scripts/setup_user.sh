#!/bin/bash

#for usr in xxx
#do
#  echo ${usr}
#  mkdir ${usr}
#  chown ${usr}:ae ${usr} -R
#done

cd ~
ln -sf /proot/tools/ohmyzsh 
ln -sf /proot/tools/cadenv/settings/.aliasesb 
ln -sf /proot/tools/cadenv/settings/.gdbinit
ln -sf /proot/tools/cadenv/settings/.zshrc 
ln -sf /proot/tools/cadenv/settings/.zshenv
ln -sf /proot/tools/cadenv/settings/.vim  
ln -sf /proot/tools/cadenv/settings/.vimrc
cp /proot/tools/cadenv/settings/.gitconfig ./
